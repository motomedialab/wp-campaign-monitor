<?php
/**
 * Campaign Monitor
 *
 * @author MotoMediaLab <https://www.motomedialab.com>
 */

if (!class_exists('WidgetAbstract')) {
    require_once(__DIR__ . '/lib/WidgetAbstract.php');
}

// create new instance of CMIntegration extending our abstract
class CMIntegration extends WidgetAbstract
{

    public static

        /**
         * Array of configurations for various sites
         *
         * @var array
         */
        $configs = [],

        // set our templates
        $templates = [];

    /**
     * Constructor class to setup up our Campaign Monitor Integration
     *
     * @return void
     */
    public function __construct()
    {
        add_action('wp_loaded', function () {
            static::$configs = apply_filters('cm:profiles', static::$configs);
            static::$templates = apply_filters('cm:templates', static::$templates);
        });

        // increase the campaign monitor timeouts to 1 minute
        // to prevent any timeouts that we may get
        define('CS_REST_SOCKET_TIMEOUT', 60);
        define('CS_REST_CALL_TIMEOUT', 60);

        // check to ensure we have ACF installed
        if (!function_exists('get_field')) {
            return;
        }

        // run parent constructor by defining slug and array of options
        parent::__construct('CMIntegration', array());

        // create meta boxes
        $this->filter('add_meta_boxes', 'add_meta_boxes');

        // create ajax action
        $this->action('wp_ajax_send_to_cm', 'wp_ajax_send_to_cm');

        // handle saving settings
        $this->action('wp_ajax_save_cm_settings', 'wp_ajax_save_cm_settings');

        // create ajax action to get lists
        $this->action('wp_ajax_get_lists_cm', 'wp_ajax_get_lists_cm');
    }

    /**
     * Determines our configuration for the current post
     *
     * @param int|null $post_id The post ID or null to auto determine
     * @param int|null $sending_profile The sending profile to return if available
     *
     * @return array Returns the configuration array
     */
    public static function get_current_configuration($post_id = null, $sending_profile = null)
    {

        // determine our post ID
        if (is_null($post_id)) {
            $post_id = get_the_id();
        }

        if ($my_meta = get_post_meta($post_id, '_cmintegration_post_settings', true)) {

            $my_config = wp_parse_args($my_meta, self::$configs[$my_meta['sending_profile']]);

            // return our custom saved meta
            return $my_config;

        } elseif (array_key_exists($sending_profile, self::$configs)) {

            // return our requested sending profile
            return self::$configs[$sending_profile];

        } elseif (is_array(self::$configs) && count(self::$configs)) {

            // return default configuration (first config in array)
            foreach (self::$configs as $config) {

                // check if we have a match
                if (array_key_exists('blog_ids', $config) && is_array($config['blog_ids'])) {
                    if (in_array(get_current_blog_id(), $config['blog_ids'])) {
                        return $config;
                    }
                }
            }
        }

        // return false
        return false;
    }

    /**
     * Pull sass variables from SASS file
     *
     * @param string $sFilename The absolute file path to get variables from
     *
     * @return stdClass          Object full of sass parameters if found
     */
    public static function get_sass_file_vars($sFilename)
    {

        // setup our variables and filters
        if (file_exists($sFilename)) {

            // create empty array of vars
            $aSassVars = array();

            // try and process our file
            preg_match_all('/(?:\$(.*?):(.*?);\n?)/i', file_get_contents($sFilename), $aMatches);

            // loop through all of our matches
            if (is_array($aMatches) && count($aMatches)) {

                // loop through our matches
                foreach ($aMatches[1] as $key => $sVariable) {

                    // trim our variable
                    $sVariable = trim($sVariable);

                    // get our value
                    $sValue = trim($aMatches[2][$key]);

                    // check if our value is a variable
                    if (substr($sValue, 0, 1) == '$') {

                        // its a variable, see if its already set
                        if (array_key_exists(substr($sValue, 1), $aSassVars)) {

                            // we have found our value
                            $sValue = $aSassVars[substr($sValue, 1)];

                        } else {

                            // we cant find this variable, skip it
                            continue;

                        }
                    }

                    // store our argument
                    $aSassVars[$sVariable] = $sValue;
                }
            }
        }

        // return our StdClass object
        return $aSassVars;
    }

    /**
     * Deletes Campaign Monitor configuration from given post
     *
     * @param int|null $post_id The post ID or null to auto determine
     *
     * @return void
     */
    public static function delete_current_configuration($post_id = null)
    {

        // determine our post ID
        if (is_null($post_id)) {
            $post_id = get_the_id();
        }

        // delete our post meta
        delete_post_meta($post_id, '_cmintegration_post_settings');
    }

    /**
     * Setup our meta boxes
     *
     * @param string $post_type Post type
     * @param WP_Post $post Post object
     */
    public function add_meta_boxes($post_type, $post)
    {

        // screens to set this meta box up on
        $screens = array('mailer');

        // loop through each screen to setup the box
        foreach ($screens as $screen) {
            // create our meta box
            add_meta_box('cm_integration', 'Campaign Monitor', array($this, 'meta_box_view'), $screen, 'side', 'high');
        }
    }

    /**
     * Output our meta box view
     *
     * @param WP_Post $post Current WP_Post object
     * @param array $meta_box Array of data for the meta box
     *
     * @return void
     */
    public function meta_box_view($post, $meta_box)
    {
        $this->load_view('cm_meta_box');
    }

    /**
     * AJAX method for updating and saving settings
     *
     * @return void
     */
    public function wp_ajax_save_cm_settings()
    {

        // strip _cm from all of our post values
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 4) == '_cm_') {
                unset($_POST[$key]);
                $_POST[substr($key, 4)] = $value;
            }
        }

        // verify our nonce
        if (!isset($_POST['nonce'], $_POST['action'], $_POST['pk']) || !wp_verify_nonce($_POST['nonce'],
                sprintf('%s_%s', $_POST['action'], $_POST['pk']))) {
            wp_send_json_error('Couldnt verify your auth key (nonce). Please reload and try again.');
        }

        // get our post ID
        $post_id = isset($_POST['pk']) ? (int)$_POST['pk'] : false;

        // remove args we dont need anymore
        unset($_POST['pk'], $_POST['nonce'], $_POST['action']);

        // get our defaults
        $aConfig = self::get_current_configuration($post_id);

        // setup our arguments to save
        $aArgs = wp_parse_args($_POST, $aConfig);
        $bReloadLists = false;

        // delete the current configuration and get new
        if ($aConfig['sending_profile'] <> $aArgs['sending_profile']) {

            self::delete_current_configuration($post_id);
            $aArgs = self::get_current_configuration($post_id, $aArgs['sending_profile']);
            $bReloadLists = true;

        } else {

            // reset any checkbox fields
            $aArgs['listids'] = isset($_POST['listids']) ? $_POST['listids'] : array();
            $aArgs['segmentids'] = isset($_POST['segmentids']) ? $_POST['segmentids'] : array();
        }

        // remove old API key arguments
        unset($aArgs['client_key'], $aArgs['api_key']);

        // update our post meta
        if (update_post_meta($post_id, '_cmintegration_post_settings', $aArgs)) {
            header('HTTP/1.1 202 Accepted');
            $aArgs['reload_lists'] = (bool)$bReloadLists;
            wp_send_json_success($aArgs);
        } else {
            wp_send_json_error('Failed to update, please reload and try again.');
        }
    }

    /**
     * Ajax method to return array of lists
     *
     * @return void
     */
    public function wp_ajax_get_lists_cm()
    {

        // get arguments
        $my_args = wp_parse_args($_REQUEST, array(
            'pk' => false,
        ));

        // get our post ID
        if (!$my_args['pk'] || !$my_post = get_post($my_args['pk'])) {
            wp_send_json_error('No post ID provided or invalid post ID.');
        }

        // get our config for the current post
        $aConfig = self::get_current_configuration($my_post->ID);

        // get our lists
        $aoLists = $this->get_lists($aConfig['client_key'], $aConfig['api_key']);

        // reset our lists
        $aoLists = array_values($aoLists);

        // determine if they are checked
        if (is_array($aoLists) && count($aoLists)) {

            if (isset($aConfig['listids']) && is_array($aConfig['listids'])) {

                // loop through all of our lists and determine if selected
                foreach ($aoLists as $key => $oList) {

                    $aoLists[$key]->selected = false;

                    // check if this list is in our array
                    if (in_array($oList->ListID, $aConfig['listids'])) {
                        $aoLists[$key]->selected = true;
                    }

                    // check the segments
                    if (is_array($oList->Segments) && isset($aConfig['segmentids'])) {

                        // loop through all of our segments
                        foreach ($oList->Segments as $key2 => $oSegment) {

                            // set to false as default
                            $aoLists[$key]->Segments[$key2]->selected = false;

                            // check if the segment is in our array
                            if (in_array($oSegment->SegmentID, $aConfig['segmentids'])) {
                                $aoLists[$key]->Segments[$key2]->selected = true;
                            }
                        }
                    }
                }
            }
        }

        // check if we have our list
        if ($aoLists) {
            wp_send_json_success($aoLists);
        }

        // if we get here, send error
        wp_send_json_error('Failed to get lists from Campaign Monitor');
    }

    /**
     * Send to campaign monitor
     *
     * @return void
     */
    public function wp_ajax_send_to_cm()
    {

        // parse our array of arguments
        $args = wp_parse_args($_POST, array(
            'post_id' => null,
        ));

        // get our config
        $aConfig = self::get_current_configuration($args['post_id']);

        // get the current post object
        if (!$post = get_post($args['post_id'])) {
            wp_send_json_error('Could not find post with the given ID.');
        }

        // check the status of the post
        if (!in_array(get_post_status($args['post_id']), array('publish', 'future'))) {
            wp_send_json_error('The post must be scheduled or published before sending to Campaign Monitor!');
        }

        // load the API so we can use it
        $oWrapper = new CS_REST_Campaigns($aConfig['client_key'], array('api_key' => $aConfig['api_key']));

        // build up our campaign name
        // SEASON - ROUND - TYPE - EVENT - DDMMYY [LANGUAGE]

        $campaign_title = html_entity_decode(get_the_title($post->ID), ENT_COMPAT, 'UTF-8');

        {
            $campaign_name = null;

            // get the post title
            $campaign_name .= $campaign_title . ' - ';

            // finally the title and language
            $campaign_name .= date('Y-m-d_H:i:s');

            // add the bloginfo to the end of the string
            $campaign_name .= ' [' . get_bloginfo('name') . ']';
        }

        // build up our campaign data
        $aCampaignData = array(
            'Subject' => $campaign_title,
            'Name' => $campaign_name,
            'HtmlUrl' => $args['previewurl'],
            'FromName' => $aConfig['from_name'],
            'FromEmail' => $aConfig['from_email'],
            'ReplyTo' => $aConfig['from_email'],
            'ListIDs' => $aConfig['listids'],
            'SegmentIDs' => $aConfig['segmentids'],
        );

        // attempt to create our campaign
        $oResult = $oWrapper->create($aConfig['client_key'], $aCampaignData);

        // check the status
        if ($oResult->http_status_code == '201') {

            // begin building our campaign details
            $campaign_data = array(
                'campaign_id' => $oResult->response,
                'summary' => $this->get_campaign_summary($oResult->response, $args['post_id']),

            );

            // parse our URL
            $aURL = parse_url($campaign_data['summary']->WorldviewURL);

            // determine our campaign ID
            $sCampaignID = explode('/', $aURL['path']);
            $sCampaignID = end($sCampaignID);

            // set our response and return it
            wp_send_json_success(sprintf('https://%s/campaigns/createSend/snapshot.aspx?cID=%s', $aURL['host'],
                $sCampaignID));

        } else {

            // return our error message
            wp_send_json_error(sprintf('HTTP Status %d: %s', $oResult->http_status_code, $oResult->response->Message));
        }
    }

    /**
     * Gets campaign summary and returns campaign array
     *
     * @param string $campaign_id Campaign ID to retreive
     * @param int|null $post_id The post ID or null to auto determine
     *
     * @return array
     */
    public function get_campaign_summary($campaign_id, $post_id = null)
    {

        // make sure we have an ID
        if (is_null($post_id)) {
            $post_id = get_the_id();
        }

        // get our config
        $aConfig = $this->get_current_configuration($post_id);

        // load our API
        $wrapper = new CS_REST_Campaigns($campaign_id, array('api_key' => $aConfig['api_key']));

        // get the result
        $result = $wrapper->get_summary();

        // if it was a success, add our web url and
        // campaign ID to the stored data
        if ($result->was_successful()) {

            // check if we can get the worldview URL
            if (property_exists($result->response, 'WorldviewURL')) {

                $matches = array();

                // get the createsend URL and true campaign ID
                if (preg_match('/^(?:[A-Z:]+\/\/)([A-Z0-9-\.]+)(?:\/reports\/wv\/i\/)([0-9A-Z]+)$/i',
                    $result->response->WorldviewURL, $matches)) {
                    // store these details
                    $result->response->WebURL = $matches[1];
                    $result->response->CampaignID = $matches[2];
                }
            }
        }

        return $result->response;
    }

    /**
     * Attempt to get all the lists that belong to our client
     *
     * @return bool|array Returns false on fail, array of lists on success
     */
    public function get_lists($client_id, $api_key)
    {

        try {
            // load the API so we can use it
            $wrapper = new CS_REST_Clients($client_id, array('api_key' => $api_key));
        }catch (Exception $e) {
            die($e->getMessage());
        }

        // try and get our lists
        $aoLists = $wrapper->get_lists();

        // make sure we had success
        if ($aoLists->was_successful()) {

            // get our lists
            $aoLists = $aoLists->response;

            // change the way the data is structured
            if (is_array($aoLists) && count($aoLists)) {
                foreach ($aoLists as $key => $oList) {

                    // create our new item
                    $aoLists[$oList->ListID] = $oList;
                    $aoLists[$oList->ListID]->Segments = array();

                    // delete old from array
                    unset($aoLists[$key]);
                }
            }

        } else {
            return false;
        }

        // try and get our segments
        $aoSegments = $wrapper->get_segments();

        // make sure we had a success
        if ($aoSegments->was_successful()) {

            // get our response
            $aoSegments = $aoSegments->response;

            // loop through each segment
            if (is_array($aoSegments) && count($aoSegments)) {
                foreach ($aoSegments as $oSegment) {
                    if (array_key_exists($oSegment->ListID, $aoLists)) {
                        $aoLists[$oSegment->ListID]->Segments[] = $oSegment;
                    }
                }
            }
        }

        // return our whole list
        return $aoLists;
    }
}

// init our CMIntegration
new CMIntegration();