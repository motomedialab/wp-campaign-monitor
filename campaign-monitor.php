<?php
/**
 * @package MotoMediaLab
 * Package URI https://www.motomedialab.com
 * @version 1.0.0
 */

/*
Plugin Name: Campaign Monitor
Description: Connecting with your audience has never been easier with Campaign Monitor’s straightforward email marketing and automation tools.
Author: MotoMediaLab
Version: 0.0.1
*/

if (!class_exists('CMIntegration')) {
    require('CampaignMonitor.php');
}