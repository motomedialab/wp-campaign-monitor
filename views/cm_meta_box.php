<?php
/**
 * Meta box widget to output
 *
 * @author Redwing Media <technical@redwing.media>
 */

$aConfig = CMIntegration::get_current_configuration();

// check if we are in draft status
if ($post->post_status == 'auto-draft'): ?>

    <p class="text-muted">
        Please save this <?= ucfirst($post->post_type) ?> to use
        Campaign Monitor.
    </p>

<?php else: ?>

    <div id="cm_settings" style="padding-top: 15px">

        <!-- hidden fields -->
        <input type="hidden" name="_cm_pk" value="<?php the_id() ?>">
        <?php wp_nonce_field('save_cm_settings_' . get_the_id(), '_cm_nonce', false, true) ?>

        <?php /* sending profile */ ?>
        <div class="acf-field">
            <div class="acf-label">
                <label for="sending_profile">Choose Sending Profile</label>
            </div>
            <div class="acf-input">
                <select name="_cm_sending_profile" id="sending_profile">
                    <?php foreach (CMIntegration::$configs as $key => $config): ?>
                        <?php if (array_key_exists('blog_ids', $config) && is_array($config['blog_ids'])) {

                            // store our blog ids as variable
                            $aBlogIds = $config['blog_ids'];

                            // check we have a restriction and act on it
                            if (count($aBlogIds) && !in_array(get_current_blog_id(), $aBlogIds)) {
                                // skip this profile because its not for this blog
                                continue;
                            }
                        } ?>
                        <option value="<?= esc_attr($key) ?>" <?= $aConfig['sending_profile'] == $key ? 'selected="selected"' : null ?>><?= esc_html($config['label']) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <?php /* template */ ?>
        <?php if (1 === 2): ?>
            <div class="acf-field">
                <div class="acf-label">
                    <label for="tpl_name">Choose Template</label>
                </div>
                <div class="acf-input">
                    <select name="_cm_tpl_name" id="tpl_name">
                        <?php foreach (CMIntegration::$templates as $sTemplate): ?>
                            <option value="<?= esc_attr($sTemplate) ?>" <?= $aConfig['tpl_name'] == $sTemplate ? 'selected="selected"' : null ?>><?= esc_html($sTemplate) ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php endif; ?>


        <?php /* from name */ ?>
        <div class="acf-field">
            <div class="acf-label">
                <label for="email_from_name">From Name</label>
            </div>
            <div class="acf-input">
                <input type="text" id="email_from_name" name="_cm_from_name"
                       value="<?= esc_attr($aConfig['from_name']) ?>"/>
            </div>
        </div>

        <?php /* from email address */ ?>
        <div class="acf-field">
            <div class="acf-label">
                <label for="email_from">From Address</label>
            </div>
            <div class="acf-input">
                <input type="text" id="email_from" name="_cm_from_email"
                       value="<?= esc_attr($aConfig['from_email']) ?>"/>
            </div>
        </div>

        <?php /* email recipients */ ?>
        <div class="acf-field" id="cm_listids">
            <div class="acf-label">
                <label for="listids">Email Recipients</label>
            </div>
            <div class="acf-input">
                <ul><?php /* this will be populated by ajax */ ?></ul>
            </div>
        </div>

        <?php /* sending buttons */ ?>
        <div style="text-align:right;clear:both;margin: 0 -12px -12px" id="major-publishing-actions">
            <?php $preview_url = add_query_arg(array('preview' => true, 'TB_iframe' => 'true'), get_permalink()) ?>
            <span class="spinner" style="float: left"></span>
            <a class="button button-secondary thickbox"
               title="<span style='line-height:30px;'><a href='#frame-togglesize' class='active'><i class='fa fa-desktop'></i> Desktop</a> | <a href='#frame-togglesize'><i class='fa fa-mobile'></i> Mobile</a> | <a href='#frame_newtab'><i class='fa fa-external-link'></i>Open in new tab</a></span>"
               href="<?= esc_url($preview_url) ?>">Preview</a>
            <a class="button button-primary act-sendcm" href="#send_to_cm" data-id="<?php the_id() ?>"
               data-previewlink="<?= $preview_url ?>">Send to CM</a>
        </div>

    </div><!--/#cm_settings-->

    <script>

        (function ($) {

            // get cm meta box
            $cm_meta_box = $('DIV#cm_integration');

            // create simple reset
            $('#cm_settings').bind('resetInputs', function (e) {
                // reset each input that has an old value
                $(':input', this).each(function (key, el) {
                    $el = $(el);
                    if ($el.data('oldval')) {
                        $el.val($el.data('oldval'));
                    }
                });
            });

            // store our old value on focus
            $('#cm_settings').on('focus', ':input', function (e) {
                $(this).data('oldval', $(this).val());
            });


            // handle the sending to CM action
            $('BODY').on('click', 'A[href=#send_to_cm]', function (e) {

                // prevent default action
                e.preventDefault();

                // store our button as a var
                $btn = $(this);

                // ignore multiple presses
                if ($btn.hasClass('disabled')) return;

                // set our post data
                var post_data = {
                    action: 'send_to_cm',
                    post_id: $btn.data('id'),
                    previewurl: $btn.data('previewlink')
                }

                // disable the link
                $btn.addClass('disbled');
                $('BODY').css({cursor: 'wait'});

                // disable inputs
                $('#cm_settings :input').addClass('disabled').prop('disabled', true);
                $('#cm_settings .spinner').css('visibility', 'visible');

                // submit our ajax request
                var jqxhr = $.post(ajaxurl, post_data);

                // on complete, remove loaders
                jqxhr.always(function () {

                    // change our button styling
                    $btn.removeClass('disabled');
                    $('BODY').css({cursor: 'auto'});

                    // re-enable our inputs
                    $('#cm_settings :input').removeClass('disabled').prop('disabled', false);
                    $('#cm_settings .spinner').css('visibility', 'hidden');

                });

                // on done, redirect to our new page
                jqxhr.done(function (res) {

                    // check for success
                    if (!res.success) {

                        // failed output the message and return false
                        alert(res.data);
                        return false;

                    } else {

                        // success, redirect to the campaign monitor
                        window.open(res.data);

                    }

                });

                // on error, output message
                jqxhr.error(function (data) {
                    alert(data);
                });

            });


            // handle saving our form settings
            $('#cm_settings').on('change', ':input', function (e) {

                // prevent default
                e.preventDefault();

                // store our inputs as a param
                var $inputs = $(':input', e.delegateTarget);
                var $spinner = $('.spinner', e.delegateTarget);

                // add action to our inputs
                var inputs = $inputs.serializeArray();
                inputs.push({'name': 'action', value: 'save_cm_settings'});

                // now submit our ajax request
                var jqxhr = $.post(ajaxurl, inputs);

                { // disable our inputs and start our spinner
                    $inputs.addClass('disabled').prop('disabled', true);
                    $spinner.css('visibility', 'visible');
                }

                // always do this after ajax
                jqxhr.always(function () {
                    $inputs.removeClass('disabled').prop('disabled', false);
                    $spinner.css('visibility', 'hidden');
                });

                // on success, do this
                jqxhr.success(function (res) {

                    // check if it was a success
                    if (!res.success) {

                        // reset our inputs and output an alert
                        $(e.delegateTarget).trigger('resetInputs');
                        alert(res.data);
                        return;

                    }

                    // process our response
                    if (res.data.reload_lists) {
                        $cm_meta_box.trigger('reload_recipients');
                    }

                    // add our new values to our fields
                    for (var property in res.data) {
                        if (res.data.hasOwnProperty(property)) {
                            $(':INPUT[name="_cm_' + property + '"]', e.delegateTarget)
                                .val(res.data[property]);
                        }
                    }


                });

                // on fail, do this
                jqxhr.fail(function () {
                    alert('Something went wrong, check your internet connection!');
                    $(e.delegateTarget).trigger('resetInputs');
                });

            });


            // thickbox frame toggle desktop & mobile
            $('BODY').on('click', '#TB_ajaxWindowTitle A[href=#frame-togglesize]', function (e) {

                // prevent default
                e.preventDefault();

                // skip this if already active
                if ($(this).hasClass('active')) return;

                // get our tb window
                var $tbWindow = $(this).parents('#TB_window');
                var tbWidth = $tbWindow.outerWidth();
                var smallWidth = 330;

                // store the initial frame width
                if (typeof $tbWindow.data('origwidth') == 'undefined') {
                    $tbWindow.data('origwidth', tbWidth);
                }

                // remove remove active classes from other links
                $(this).parent().find('A').removeClass('active');
                $(this).addClass('active');

                // check if we are on small version
                if (tbWidth == smallWidth) {
                    desiredWidth = $tbWindow.data('origwidth');
                } else {
                    desiredWidth = smallWidth;
                }

                // set our window width
                $tbWindow.css({
                    'width': desiredWidth,
                    'margin-left': -Math.abs(desiredWidth / 2)
                });

                // set our iframe width
                $tbWindow.find('#TB_iframeContent').css({
                    'width': desiredWidth
                });

            });


            // handle changing of from name & recipients
            $cm_meta_box.on('reload_recipients', function () {

                // run ajax call
                var jqxhr = $.getJSON(ajaxurl, {
                    action: 'get_lists_cm',
                    pk: '<?=get_the_id()?>',
                    blog_id: $('SELECT#site_id').val()
                });

                console.log(jqxhr);

                // show the spinner, slide the UL up and empty it
                $('#cm_listids UL').slideUp().empty();
                $('#cm_listids .acf-label SPAN.spinner').show();

                // on success, load our lists
                jqxhr.success(function (res) {

                    if (res.success) {

                        // check we have some lists
                        if (res.data.length) {

                            // loop through each item
                            $(res.data).each(function (key1, item) {

                                // create our list items
                                $('#cm_listids .acf-input UL').append(
                                    '<li class="List"><input type="checkbox" name="_cm_listids[]" ' + (item.selected ? 'checked="checked"' : null) + ' id="checkbox_list_id_' + key1 + '" value="' + item.ListID + '" class="recipient-item">' +
                                    '<label for="checkbox_list_id_' + key1 + '"><b>' + item.Name + '</b></label></li>'
                                );

                                // create our segments
                                if (item.Segments.length) {
                                    $(item.Segments).each(function (key2, segment) {

                                        $('#cm_listids .acf-input UL').append(
                                            '<li class="Segment">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="_cm_segmentids[]" ' + (segment.selected ? 'checked="checked"' : null) + ' id="checkbox_list_id_' + key1 + '_' + key2 + '" value="' + segment.SegmentID + '">' +
                                            '<label for="checkbox_list_id_' + key1 + '_' + key2 + '">' + segment.Title + '</label></li>'
                                        );
                                    });
                                }
                            });

                        } else {
                            // no lists found
                            $('#cm_listids .acf-input').html('<p>No lists found. Please login to Campaign Monitor, create at least one list and try again.</p>');
                            return;
                        }
                    } else {
                        // got a json error
                        alert(res.data);
                    }
                });

                // hide the spinner when the request is complete
                jqxhr.always(function () {

                    // request complete, remove the spinner
                    $('#cm_listids .acf-label SPAN.spinner').hide();

                    // slide the list back down
                    $('#cm_listids UL').slideDown();
                });

                // on fail, output error message
                jqxhr.fail(function (data) {
                    $('#cm_listids .acf-input').html('<p>Campaign Monitor lists failed to load. Please try refreshing the page.</p>');
                });

            }).trigger('reload_recipients'); // trigger on load


        })(jQuery);

    </script>

    <style>

        #TB_ajaxWindowTitle A.active {
            color: #333;
            text-decoration: none;
        }

    </style>

<?php endif; ?>