<?php
/**
 * Abstract class to extend the default
 * functionality for plugins and widgets
 *
 * @author Redwing Media <technical@redwingmedia.co.uk>
 */

abstract class WidgetAbstract {

    public
        /**
         * Variable to store the path of the plugin directory
         * @var string
         */
        $plugin_dir = null,

        /**
         * Variable to store the url of the plugin directory
         * @var [type]
         */
        $plugin_url = null;

    protected
        /**
         * Array to store all plugin settings
         * @var array
         */
        $_options = array (),
        /**
         * Admin notice ID
         * @var string
         */
        $_adminNoticeID = 'session_notices',
        /**
         * Prefix for option parameters
         * @var string
         */
        $_optionPrefix = null,
        /**
         * Array to store magic getter/setter params
         * @var array
         */
        $__get = array ();

    private

        /**
         * The plugin slug to check for updates
         * @var string
         */
        $_plugin_slug = null,

        /**
         * Create reflection instance for gathering additional data
         * @var null|object
         */
        $_reflection = null;


    /**
     * Supr3meBaseAbstract Plugin constructor class
     * @param string $plugin_slug  Plugin slug for settings saved to database
     * @param array  $options      Array of permittable options for this plugin
     */
    public function __construct ($plugin_slug, array $options) {

        // if the session has not been started, start it
        if(session_status() == PHP_SESSION_NONE) session_start();

        // set our reflection class so we can get more details
        // from our child class
        $this->_reflection = new ReflectionClass(get_class($this));

        // set the plugin directory string
        $this->plugin_dir = dirname($this->_reflection->getFileName()) . DIRECTORY_SEPARATOR;

        // set the plugin url string
        $this->plugin_url = plugin_dir_url($this->_reflection->getFileName());

        // set our plugin slug and generate our option prefix
        $this->_optionPrefix = rtrim('supr3me_' . $plugin_slug, '_') . '_';
        $this->_plugin_slug  = 'supr3me-' . trim($plugin_slug);

        // populate our array of options
        $this->_options = $options;
        $this->_options = $this->get_option();

        // create filter for admin notices
        $this->action(array('admin_notices', 'network_admin_notices'), 'notices_display', 10, 2);

        // return current instance
        return $this;
    }

    /**
     * Magic __get() method to get undefined params
     * @param  string $param_name   Parameter name
     * @return mixed                Value of the parameter
     */
    public function __get($param_name) {

        // create $return param
        $return = false;

        // if we have already instantiated this param, return it
        if(array_key_exists($param_name, $this->__get)) {
            return $this->__get[$param_name];
        }

        // return our populated $return and store
        // it in our caching parameter
        return $this->__get[$param_name] = $return;
    }

    /**
     * Magic __set() method to set magic params
     * @param string $param_name    Parameter name
     * @param mixed $param_value    Parameter value
     */
    public function __set($param_name, $param_value) {
        // set the param against our $this->__get array
        return $this->__get[$param_name] = $param_value;
    }

    /**
     * Update plugin options in the database
     * @param  string $key          Key to store the option under
     * @param  mixed $value         Value to store against the option
     * @return bool                 Returns true on success, false on failure
     */
    public function update_option ($key, $value) {

        // check if the option exists
        if(array_key_exists($key, $this->_options)) {

            // attempt to update the option
            if(update_site_option($this->_optionPrefix . $key, $value)) {

                // set option on our options array
                $this->_options[$key] = $value;
                return true;
            }
        }

        // if we get to this point, update failed
        return false;
    }

    /**
     * Gets plugin option / options from the database
     * @return array
     */
    public function get_option ($key = null) {

        // if the key is not null, attempt to return a single value
        if(!is_null($key)) {
            if(isset($this->_options[$key]) && !is_null($this->_options[$key])) {
                // return our cached option
                return $this->_options[$key];
            } else {
                // get it from the database and return it
                return get_site_option($this->_optionPrefix . $key);
            }
        }

        // loop through each option and get it from the database
        foreach($this->_options as $key => $value) {

            // try and get this option from the database
            $db_loaded = get_site_option($this->_optionPrefix . $key);

            // get option with a default
            $this->_options[$key] = ($db_loaded !== false ? $db_loaded : $value);
        }

        // return the resulting array
        return $this->_options;
    }

    /**
     * Sets a notice for the current session
     * @param string $message       Message to output
     * @param string $class         Class of the message. Should be one of updated, error or updated-nag
     * @return void
     */
    public function notice_set ($message, $class = 'updated') {

        // @see http://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices

        // set the session parameter
        $_SESSION[$this->_adminNoticeID][] = array (
            'message'   => $message,
            'class'     => $class,
        );
    }

    /**
     * Outputs admin notices using the admin_notices hook
     * @return void
     */
    public function notices_display () {

        // @see http://codex.wordpress.org/Plugin_API/Action_Reference/admin_notices

        // dont continue if our notices are not an array
        if(!isset($_SESSION[$this->_adminNoticeID]) || !is_array($_SESSION[$this->_adminNoticeID])) return;

        // if our session is an array, loop through each message
        foreach($_SESSION[$this->_adminNoticeID] as $key => $notice) {

            // output our message
            echo "<div class='{$notice['class']}'><p>{$notice['message']}</p></div>";

            // unset it as its been shown
            unset($_SESSION[$this->_adminNoticeID][$key]);
        }
    }

    /**
     * Helper function for WordPress actions
     * @param  string|array $hook   Name of the filter to use
     * @param  string|null $method  The method to call, defaults to $hook
     * @param  int $priority        Action priority
     * @return void
     */
    public function action ($hook, $method = null, $priority = 10) {

        // convert the $hook string into an array
        if(!is_array($hook))
            $hook = array($hook);

        // return private hook method with action flag
        foreach($hook as $hook_single)
            $this->_hook('action', $hook_single, $method, $priority);
    }

    /**
     * Helper function for WordPress filters
     * @param  string $hook         Name of the filter to use
     * @param  string|null $method  The method to call, defaults to $hook
     * @param  int $priority        Filter priority
     * @return void
     */
    public function filter ($hook, $method = null, $priority = 10) {

        // convert the $hook string into an array
        if(!is_array($hook))
            $hook = array($hook);

        // return private hook method with filter flag
        foreach($hook as $hook_single)
            $this->_hook('filter', $hook_single, $method, $priority);
    }

    /**
     * Method to enqueue theme Javascripts
     * @param  string $src       Source file, e.g. general.js
     * @param  array  $deps      Array of dependencies
     * @param  bool $in_footer   Set to true to load in footer instead of header
     * @return bool
     */
    public function enqueue_script ($src, $deps = array (), $in_footer = false) {

        // try and get the file version
        $ver = file_exists($this->plugin_dir . 'javascripts/' . $src) ? filemtime($this->plugin_dir . 'javascripts/' . $src) : time();

        // enqueue the script
        return wp_enqueue_script(sanitize_title($src), $this->plugin_url . 'javascripts/' . $src, $deps, $ver, $in_footer);
    }

    /**
     * Method to enqueue theme Stylesheets
     * @param  string $src       Source file, e.g. screen.css
     * @param  array  $deps      Array of dependencies
     * @param  string $media     Media type, one of 'all', 'screen', 'handheld' or 'print'
     * @return bool
     */
    public function enqueue_style ($src, $deps = array (), $media = 'screen') {

        // try and get the file version
        $ver = file_exists($this->plugin_dir . 'stylesheets/' . $src) ? filemtime($this->plugin_dir . 'stylesheets/' . $src) : time();

        // enqueue the stylesheet
        return wp_enqueue_style(sanitize_title($src), $this->plugin_url . 'stylesheets/' . $src, $deps, $ver, $media);
    }

    /**
     * Basic method to load view file
     * @param  string $name         Name of the view to load
     * @param  array $variables     Array of variables to make available to view
     * @return void
     */
    public function load_view ($name, $variables = array()) {

        // get global $post
        global $post;

        // set options as an accesible variable
        $options = $this->_options;

        // extract variables as parameters and then remove $variables
        extract($variables);
        unset($variables);

        // load in our view file
        require_once($this->plugin_dir . '/views/' . $name . '.php');

        // unset our options
        unset($options);
    }

    /**
     * PRIVATE METHODS FROM THIS POINT ONWARDS
     */


    /**
     * Registers WordPress hooks and actions
     * @param  string $type         Either action or filter type
     * @param  string $hook         The WordPress filter/action
     * @param  string|null $method  The method to call
     * @param  int $priority        Action/filter priority
     * @return mixed
     */
    private function _hook ($type, $hook, $method = null, $priority = 10) {

        // check if we are trying to run a valid type (default to action)
        if(!in_array($type, array('filter', 'action'))) $type = 'action';

        // prefix the type
        $type = 'add_' . $type;

        // set the method to be the hook if not defined
        if(is_null($method)) $method = $hook;

        // return the result of the WordPress filter/action
        return $type($hook, is_object($method) ? $method :  array($this, $method), $priority, 999);
    }
}